using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimulationCharacter : MonoBehaviour
{
  Animator _animator;
  [SerializeField] string[] characterAnimation;
  [SerializeField] GameObject bowBtn, takeArrowBtn, releaseArrowBtn, popUpHandler;
  int countAnimTakeArrow, countAnimReleaseArrow, counter;

  // Start is called before the first frame update
  void Start()
  {
    _animator = GetComponent<Animator>();
    counter = 0;
    bowBtn.SetActive(true);
    takeArrowBtn.SetActive(false);
    releaseArrowBtn.SetActive(false);
    countAnimTakeArrow = 1;
    countAnimReleaseArrow = 2;
  }

  public void BowAim()
  {
    counter++;
    _animator.Play(characterAnimation[0]);
    bowBtn.SetActive(false);
    takeArrowBtn.SetActive(true);
  }

  public void TakeArrow()
  {
    counter++;
    _animator.Play(characterAnimation[countAnimTakeArrow]);
    countAnimTakeArrow += 2;
    takeArrowBtn.SetActive(false);
    releaseArrowBtn.SetActive(true);
  }

  public void ReleaseArrow()
  {
    counter++;
    _animator.Play(characterAnimation[countAnimReleaseArrow]);
    countAnimReleaseArrow += 2;
    takeArrowBtn.SetActive(true);
    releaseArrowBtn.SetActive(false);
  }

  private void Update()
  {
    if (counter > characterAnimation.Length)
    {
      popUpHandler.SetActive(true);
    }
  }

  public void NextButton()
  {
    SceneManager.LoadScene("Stage 1");
  }

  public void RetryButton()
  {
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }
}
