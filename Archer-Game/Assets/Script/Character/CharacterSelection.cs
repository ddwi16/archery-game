using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour
{
  public GameObject[] playerSelection;
  int selectedCharacter;

  private void Awake()
  {
    selectedCharacter = PlayerPrefs.GetInt("SelectedCharacter", 0);
    foreach (GameObject player in playerSelection)
    {
      player.SetActive(false);
    }

    playerSelection[selectedCharacter].SetActive(true);
  }

  public void NextBtn()
  {
    playerSelection[selectedCharacter].SetActive(false);
    selectedCharacter++;
    if (selectedCharacter == playerSelection.Length)
    {
      selectedCharacter = 0;
    }
    playerSelection[selectedCharacter].SetActive(true);
  }

  public void PreviousBtn()
  {
    playerSelection[selectedCharacter].SetActive(false);
    selectedCharacter--;
    if (selectedCharacter == -1)
    {
      selectedCharacter = playerSelection.Length - 1;
    }
    playerSelection[selectedCharacter].SetActive(true);
  }

  public void SubmitBtn()
  {
    PlayerPrefs.SetInt("SelectedCharacter", selectedCharacter);
    SceneManager.LoadScene("Simulation");
  }
}
