using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSpawner : MonoBehaviour
{
  public GameObject[] playerPrefabs;
  int characterIndex;

  private void Awake()
  {
    characterIndex = PlayerPrefs.GetInt("SelectedCharacter", 0);
    playerPrefabs[characterIndex].SetActive(true);
  }
}
