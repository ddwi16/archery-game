using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreUpdate : MonoBehaviour
{
  int totalScore;
  public BandolanBehaviorBody bandolanBehaviorBody;
  public BandolanBehaviorHead bandolanBehaviorHead;
  [SerializeField] TextMeshProUGUI totalScoreTxt;

  private void Update()
  {
    totalScore = bandolanBehaviorHead.getScoreHead() + bandolanBehaviorBody.getScoreBody();
    totalScoreTxt.text = totalScore.ToString();
  }

  public int getTotalScore()
  {
    return totalScore;
  }
}
