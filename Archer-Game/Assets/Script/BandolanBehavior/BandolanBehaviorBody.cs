using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BandolanBehaviorBody : MonoBehaviour
{
  int score = 0;
  void OnCollisionEnter2D(Collision2D other)
  {
    if (other.gameObject.tag == "Weapon")
    {
      score += 1;
    }
  }

  public int getScoreBody()
  {
    return score;
  }
}
