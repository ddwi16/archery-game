using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BandolanBehaviorHead : MonoBehaviour
{
  int score = 0;
  GameObject prefabs;
  [SerializeField] GameObject[] hitPrefab;
  [SerializeField] GameObject sfxBandolan;
  [SerializeField] GameObject parentHolder;

  void OnCollisionEnter2D(Collision2D other)
  {
    if (other.gameObject.tag == "Weapon")
    {
      score += 3;
      int spawnBebungah = Random.Range(0, hitPrefab.Length);

      prefabs = Instantiate(hitPrefab[spawnBebungah]);
      Destroy(prefabs, 2f);
      prefabs.transform.SetParent(parentHolder.transform);

      GameObject sfxPrefabs = Instantiate(sfxBandolan);
      Destroy(sfxPrefabs, 1f);
    }
  }

  public int getScoreHead()
  {
    return score;
  }
}
