using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimShootArrow : MonoBehaviour
{
  private Animation _animation;

  // Start is called before the first frame update
  void Start()
  {
    _animation = GetComponent<Animation>();
  }

  public void RaiseHandBtn()
  {
    _animation.Play("Arjuna_Aiming");
  }

  public void PickArrowBtn()
  {
    _animation.Play("Arjuna_PickArrow");
  }

  public void ShootArrowBtn()
  {
    _animation.Play("Arjuna_ShootArrow");
  }
}
