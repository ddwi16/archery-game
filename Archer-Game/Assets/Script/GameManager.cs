using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject selectedPlayer;
    public GameObject mainPlayer;

    private Sprite playerSprite;

    private void Start() {
        playerSprite = selectedPlayer.GetComponent<SpriteRenderer>().sprite;
        mainPlayer.GetComponent<SpriteRenderer>().sprite = playerSprite;
    }
}
