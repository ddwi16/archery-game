using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
  [SerializeField] GameObject pauseGO;

  // Update is called once per frame
  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape))
    {
      pauseGO.SetActive(true);
      Time.timeScale = 0f;
    }
  }

  public void ButtonYesPause()
  {
    Time.timeScale = 1f;
    SceneManager.LoadScene("Main Menu");
  }

  public void ButtonNoPause()
  {
    pauseGO.SetActive(false);
    Time.timeScale = 1f;
  }
}
