using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PopUpManagerAgeng : MonoBehaviour
{
  [SerializeField] GameObject trophyHandler, goldTrophy, silverTrophy, bronzeTrophy, txtInfo;
  [SerializeField] TextMeshProUGUI scoreValueTxt;
  [SerializeField] string nextStage;
  [SerializeField] ScoreUpdate scoreUpdate;

  // Update is called once per frame
  void Update()
  {
    int score = scoreUpdate.getTotalScore();
    if (score > 100)
    {
      goldTrophy.SetActive(true);
    }
    else if (score >= 75 && score <= 100)
    {
      silverTrophy.SetActive(true);
    }
    else if (score >= 50 && score < 75)
    {
      bronzeTrophy.SetActive(true);
    }
    else if (score < 50)
    {

    }
    scoreValueTxt.text = "score " + score.ToString();
  }

  public void NextButton()
  {
    SceneManager.LoadScene(nextStage);
  }

  public void RetryButton()
  {
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }
}
