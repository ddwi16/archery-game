using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationManager : MonoBehaviour
{
  [SerializeField] GameObject panelTransition;
  [SerializeField] GameObject[] playerSitDown;
  [SerializeField] GameObject[] playerStandUp;
  [SerializeField] GameObject arrowContainer;
  [SerializeField] GameObject textChooseArrow;
  [SerializeField] GameObject[] simulationButton;
  [SerializeField] GameObject[] backgroundSimulation;
  [SerializeField] Transform[] spawnPointPopUpBow;
  [SerializeField] GameObject[] popUpBowPrefabs;
  int selectedCharacter;

  public void BowPickSmall()
  {
    GameObject go = Instantiate(popUpBowPrefabs[0], spawnPointPopUpBow[0].position, Quaternion.identity);
    go.transform.parent = spawnPointPopUpBow[0].transform;
    Destroy(go, 1.5f);
  }

  public void BowPickMedium()
  {
    GameObject go = Instantiate(popUpBowPrefabs[1], spawnPointPopUpBow[1].position, Quaternion.identity);
    go.transform.parent = spawnPointPopUpBow[1].transform;
    Destroy(go, 1.5f);

    selectedCharacter = PlayerPrefs.GetInt("SelectedCharacter", 0);
    StartCoroutine(AnimationTransition());
  }

  public void BowPickBig()
  {
    GameObject go = Instantiate(popUpBowPrefabs[2], spawnPointPopUpBow[2].position, Quaternion.identity);
    go.transform.parent = spawnPointPopUpBow[2].transform;
    Destroy(go, 1.5f);
  }

  IEnumerator AnimationTransition()
  {
    yield return new WaitForSeconds(1f);
    panelTransition.SetActive(true);

    yield return new WaitForSeconds(1.5f);
    textChooseArrow.SetActive(false);
    arrowContainer.SetActive(false);
    foreach (GameObject player in playerStandUp)
    {
      player.SetActive(false);
    }
    playerSitDown[selectedCharacter].SetActive(true);
    backgroundSimulation[0].SetActive(false);
    backgroundSimulation[1].SetActive(true);

    yield return new WaitForSeconds(2f);
    simulationButton[selectedCharacter].SetActive(true);
    panelTransition.SetActive(false);
  }
}
