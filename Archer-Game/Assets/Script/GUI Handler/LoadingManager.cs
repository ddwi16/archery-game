using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour
{
  [SerializeField] string nameScene;

  private void Update()
  {
    StartCoroutine(LoadAsync(nameScene));
  }

  IEnumerator LoadAsync(string name)
  {
    yield return new WaitForSeconds(5.5f);
    SceneManager.LoadScene(name);
  }
}
