using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PopUpManagerAlit : MonoBehaviour
{
  [SerializeField] GameObject winStage, loseStage;
  [SerializeField] GameObject arjunaItem, srikandiItem;
  [SerializeField] TextMeshProUGUI scoreValueTxt;
  [SerializeField] string nextStage;
  [SerializeField] ScoreUpdate scoreUpdate;
  private int characterIndex;

  private void Awake()
  {
    characterIndex = PlayerPrefs.GetInt("SelectedCharacter", 0);
  }

  // Update is called once per frame
  void Update()
  {
    int score = scoreUpdate.getTotalScore();
    if (score >= 15)
    {
      winStage.SetActive(true);
      loseStage.SetActive(false);

      if (characterIndex == 0)
      {
        arjunaItem.SetActive(true);
        srikandiItem.SetActive(false);
      }
      else
      {
        arjunaItem.SetActive(false);
        srikandiItem.SetActive(true);
      }
    }
    else
    {
      winStage.SetActive(false);
      loseStage.SetActive(true);
    }
    scoreValueTxt.text = "score " + score.ToString();
  }

  public void NextButton()
  {
    SceneManager.LoadScene(nextStage);
  }

  public void RetryButton()
  {
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }
}
