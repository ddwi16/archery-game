using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
  [SerializeField] GameObject guideLayer, mainLayer, quitGameLayer;
  [SerializeField] GameObject[] slideGo;
  private int selectedSlide = 0;

  private void Start()
  {
    mainLayer.SetActive(true);
    guideLayer.SetActive(false);
  }

  public void PlayBtn()
  {
    SceneManager.LoadScene("CharSelection");
  }

  public void ExitBtn()
  {
    quitGameLayer.SetActive(true);
  }

  public void GuideBtn()
  {
    quitGameLayer.SetActive(false);
    mainLayer.SetActive(false);
    guideLayer.SetActive(true);
  }

  public void BackBtn()
  {
    quitGameLayer.SetActive(false);
    mainLayer.SetActive(true);
    guideLayer.SetActive(false);
  }

  public void NoButton()
  {
    quitGameLayer.SetActive(false);
  }

  public void YesButton()
  {
    Application.Quit();
  }

  public void NextBtn()
  {
    slideGo[selectedSlide].SetActive(false);
    selectedSlide++;
    if (selectedSlide == slideGo.Length)
    {
      selectedSlide = 0;
    }
    slideGo[selectedSlide].SetActive(true);
  }

  public void PreviousBtn()
  {
    slideGo[selectedSlide].SetActive(false);
    selectedSlide--;
    if (selectedSlide == -1)
    {
      selectedSlide = slideGo.Length - 1;
    }
    slideGo[selectedSlide].SetActive(true);
  }
}
