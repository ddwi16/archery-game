using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioHandler : MonoBehaviour
{
  static AudioHandler instance;
  [SerializeField] GameObject _audioBGM, _audioGladhenAgeng;

  private void Awake()
  {
    Singleton();
  }

  // Update is called once per frame
  void Update()
  {
    Scene scene = SceneManager.GetActiveScene();
    if (scene.name == "Stage 2")
    {
      _audioBGM.SetActive(false);
      _audioGladhenAgeng.SetActive(true);
    }
    else
    {
      _audioBGM.SetActive(true);
      _audioGladhenAgeng.SetActive(false);
    }
  }

  void Singleton()
  {
    if (instance != null && instance != this)
    {
      Destroy(this);
      return;
    }
    else
    {
      instance = this;
    }

    DontDestroyOnLoad(this);
  }
}
