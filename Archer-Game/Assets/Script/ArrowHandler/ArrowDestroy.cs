using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowDestroy : MonoBehaviour
{
  ArrowController arrowController;
  ArrowSpawner arrowSpawner;

  // Start is called before the first frame update
  void Start()
  {
    arrowController = GameObject.FindObjectOfType<ArrowController>();
    arrowSpawner = GameObject.FindObjectOfType<ArrowSpawner>();
  }

  // Update is called once per frame
  void Update()
  {

  }

  private void OnCollisionEnter2D(Collision2D other)
  {
    if (other.gameObject.tag == "Head" ||
      other.gameObject.tag == "Body" ||
      other.gameObject.tag == "Ground")
    {
      Destroy(arrowSpawner.arrowSpawn, 2f);
      arrowController._rigidbody2D.bodyType = RigidbodyType2D.Static;
    }
  }
}
