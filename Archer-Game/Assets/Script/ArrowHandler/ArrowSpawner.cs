using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ArrowSpawner : MonoBehaviour
{
  [SerializeField] GameObject[] character;
  private int selectedCharacter;
  bool isAiming;

  public GameObject[] arrowIcon;
  public GameObject arrowPrefabs;
  public Transform spawnPosition;
  GameObject arrowObj;
  public GameObject arrowSpawn;

  [SerializeField] TextMeshProUGUI roundTxt;
  [SerializeField] int roundGame = 5;
  [SerializeField] GameObject spawnArrowHolder;
  [SerializeField] GameObject popUpPanel;
  int countRound = 1;
  int countArrow = -1;

  private void Awake()
  {
    selectedCharacter = PlayerPrefs.GetInt("SelectedCharacter", 0);
  }

  private void Update()
  {
    arrowObj = GameObject.Find("ArrowObj(Clone)");

    if (arrowObj == null)
    {
      if (countArrow < 4)
      {
        character[selectedCharacter].GetComponent<Animator>().Play("Aiming", -1, 0f);
        arrowSpawn = Instantiate(arrowPrefabs, spawnPosition.position, spawnPosition.rotation);
        arrowSpawn.transform.parent = spawnArrowHolder.transform;
        arrowSpawn.transform.localScale = spawnArrowHolder.transform.localScale;
        countArrow++;
        arrowIcon[countArrow].SetActive(false);
      }
    }

    if (countArrow >= 4)
    {
      countArrow = 0;

      for (int i = 0; i < arrowIcon.Length; i++)
      {
        arrowIcon[i].SetActive(true);
      }
      countRound += 1;
      roundTxt.text = countRound.ToString();

      arrowIcon[countArrow].SetActive(false);
    }

    if (countRound > roundGame)
    {
      Debug.Log("Sudah Selesai");
      popUpPanel.SetActive(true);
    }
  }
}
