using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
  [HideInInspector] public Rigidbody2D _rigidbody2D;
  [HideInInspector] public Collider2D _collider2D;

  [HideInInspector] public Vector3 pos { get { return transform.position; } }

  // Start is called before the first frame update
  void Start()
  {
    _rigidbody2D = GetComponent<Rigidbody2D>();
    _collider2D = GetComponent<Collider2D>();
    _rigidbody2D.bodyType = RigidbodyType2D.Static;
  }

  public void Push(Vector2 force)
  {
    _rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
    _rigidbody2D.AddForce(force, ForceMode2D.Impulse);
  }
}
