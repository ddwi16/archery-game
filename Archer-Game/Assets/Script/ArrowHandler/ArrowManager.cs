using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowManager : MonoBehaviour
{
  [SerializeField] GameObject sfxArrowShoot;
  Camera _camera;

  public ArrowController[] arrows;
  private ArrowController defaultArrow;
  public Trajectory trajectory;
  [SerializeField] float pushForce = 4f;

  bool isDragging;

  Vector2 startPoint;
  Vector2 endPoint;
  Vector2 direction;
  Vector2 force;
  float distance;

  // Start is called before the first frame update
  void Start()
  {
    _camera = Camera.main;
  }

  // Update is called once per frame
  void Update()
  {
    Vector2 pos = _camera.ScreenToWorldPoint(Input.mousePosition);
    if (Input.GetMouseButtonDown(0))
    {
      defaultArrow = null;
      for (int i = 0; i < arrows.Length; i++)
      {
        if (arrows[i]._collider2D == Physics2D.OverlapPoint(pos))
        {
          // Time.timeScale = 0.1f;
          isDragging = true;
          defaultArrow = arrows[i];
          OnDragStart();
        }
      }
    }

    if (Input.GetMouseButtonUp(0))
    {
      if (defaultArrow != null)
      {
        // Time.timeScale = 1f;
        isDragging = false;
        GameObject sfxPrefab = Instantiate(sfxArrowShoot);
        Destroy(sfxPrefab, 2f);
        OnDragEnd();
      }
    }

    if (isDragging)
    {
      OnDrag();
    }
  }

  #region Drag
  private void OnDragStart()
  {
    startPoint = _camera.ScreenToWorldPoint(Input.mousePosition);

    StartCoroutine(TrajectoryShow());
  }
  private void OnDrag()
  {
    endPoint = _camera.ScreenToWorldPoint(Input.mousePosition);
    distance = Vector2.Distance(startPoint, endPoint);
    direction = (startPoint - endPoint).normalized;
    force = distance * direction * pushForce;

    trajectory.UpdateDots(defaultArrow.pos, force);
  }
  private void OnDragEnd()
  {
    defaultArrow.Push(force);

    trajectory.Hide();
  }
  #endregion

  IEnumerator TrajectoryShow()
  {
    yield return new WaitForSeconds(.1f);
    Handheld.Vibrate();
    trajectory.Show();
  }
}
